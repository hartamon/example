import { createWebHistory, createRouter } from "vue-router";

const routes = [
    {
        path: '/spa/get',
        component: () => import('./components/Get.vue'),
        name: 'get.index'
    },
    {
        path: '/spa/dropzone',
        component: () => import('./components/Dropzone.vue'),
        name: 'dropzone'
    },
    {
        path: '/spa/user/login',
        component: () => import('./components/Login.vue'),
        name: 'user.login'
    },
    {
        path: '/spa/user/registration',
        component: () => import('./components/Registration.vue'),
        name: 'user.registration'
    },
    {
        path: '/spa/user/personal',
        component: () => import('./components/Personal.vue'),
        name: 'user.personal'
    }


]



const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(),
    routes, // short for `routes: routes`
})

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('x_xsrf_token');
    if (!token) {
        if (to.name === 'user.login' || to.name === 'user.registration') {
            return next()
        } else {
            return next( {
                name: 'user.login'
            })
        }
    } else {
        if (to.name === 'user.login' || to.name === 'user.registration') {
            return next( {
                name: 'user.personal'
            })
        }
    }
    next()
});

export default router
